/**
 * \brief Test parser accessors
 * \file
 *
 */

#include <string>
#include <memory>

#include "test_runner.hpp"
#include "../tcli/tcli.hpp"

struct parser_tag {};
using Tests = test::test_runner<parser_tag,200>;

using str_vec = std::vector<std::string>;

namespace test {

    struct Int {
        using type = int;
        str_vec flags = {"--int"};
    };

    struct Double {
        using type = double;
        str_vec flags = {"--double"};
    };

    /**
     * \brief Check that empty args works
     *
     * Expected behaviour:
     *   - does not throw any exception
     */
    template<> template<>
    void Tests::test<0>() {
        str_vec args{"prog_name"};
        auto parser = tcli::make_parser(Int{});

        try {
            parser.parse(args);
        } catch (std::exception& e) {
            assert(false, std::string("Unexpected exception: ") + e.what());
            return;
        }

        assert_equal(parser.get<Int>(), 0, "Incorrect value in Int");
    }

    /**
     * \brief Check that parser with int parameter works
     *
     * Expected behaviour:
     *   - does not throw any exception
     *   - int value is correct
     */
    template<> template<>
    void Tests::test<1>() {
        str_vec args{"prog_name", "--int", "24"};
        auto parser = tcli::make_parser(Int{});

        try {
            parser.parse(args);
        } catch (std::exception& e) {
            assert(false, std::string("Unexpected exception: ") + e.what());
            return;
        }

        assert_equal(parser.get<Int>(), 24, "Int value not parsed correctly.");
    }


    /**
     * \brief Check that partially parsed input throws an error
     */
    template<> template<>
    void Tests::test<2>() {
        str_vec args{"prog_name", "--int", "24.5"};
        auto parser = tcli::make_parser(Int{});

        try {
            parser.parse(args);
            assert(false, "Parse should throw an exception about partial parse of argument");
        } catch (tcli::partial_parse& e) {}
    }

    /**
     * \brief Check that parser with double parameter works
     *
     * Expected behaviour:
     *   - does not throw any exception
     *   - double value is correct
     */
    template<> template<>
    void Tests::test<3>() {
        str_vec args{"prog_name", "--double", "3.14"};
        auto parser = tcli::make_parser(Double{});

        try {
            parser.parse(args);
        } catch (std::exception& e) {
            assert(false, std::string("Unexpected exception: ") + e.what());
            return;
        }

        assert_equal(parser.get<Double>(), 3.14, "Double value not parsed correctly.");
    }

    /**
     * \brief Unknown parameter must throw an exception
     *
     * Expected behaviour:
     *   - throw an exception for unknown parameter
     */
    template<> template<>
    void Tests::test<4>() {
        str_vec args{"prog_name", "--doube", "3.14"};
        auto parser = tcli::make_parser(Int{});

        try {
            parser.parse(args);
            assert(false, std::string("Parser should throw an exception for unknown parameters."));
        } catch (tcli::parse_error& e) {}
    }


}


int main() {
    return Tests{}.run();
}
