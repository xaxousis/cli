#ifndef TEST_RUNNER_HPP
#define TEST_RUNNER_HPP

#include <iostream>
#include <sstream>
#include <type_traits>

#include "../tcli/integer_sequence.hpp"

namespace test {

template<class tag = void, std::size_t MaxCount = 50>
struct test_runner {

    struct no_test {};

    int test_count = 0;
    int success_count = 0;
    int test_id = 0;

    bool current_test = true;

    constexpr static bool id_too_large (std::size_t i) {
        return i >= MaxCount;
    }

    template<std::size_t I, typename std::enable_if<!id_too_large(I)>::type* = nullptr>
    void test() {
        throw no_test{};
    }

    bool run() {
        return run_impl(cpp::make_index_sequence<MaxCount>());
    }

    template<std::size_t... Is>
    bool run_impl(cpp::index_sequence<Is...>) {
        bool res = true;
        auto l = {true, (res &= run_one<Is>())...};
        (void)l;
        std::cout << success_count << "/" << test_count << " tests passed.\n";
        return !res;
    }


    template<std::size_t I>
    bool run_one() {
        this->current_test = true;
        this->test_id = I;
        try {
            test<I>();
            ++test_count;
            if(this->current_test) {
                ++success_count;
            }
        } catch(no_test) {}
        return this->current_test;
    }

    bool assert(bool b, const std::string& message = "no message") {
        if(!b) {
            this->current_test = false;
            std::cout << "test id " << this->test_id << ": ";
            std::cout << message << '\n';
        }
        return b;
    }

    template<class A, class B>
    bool assert_equal(const A& a, const B& b, const std::string& msg = "no message") {
        return assert(a == b, msg + '\n' + test_runner::msg("Expected '", b, "',got '", a,'\''));
    }


    template<class... Args>
    static std::string msg(Args... args) {
        std::stringstream sstr;
        auto l = {0, (sstr << args, 0)...};
        (void)l;
        return sstr.str();
    }

};


}




#endif /* TEST_RUNNER_HPP */
