/**
 * \brief Test parser parameter overloads
 * \file
 *
 * The parse_parameter group of functions calls the corresponding parse steps
 * once a parameter has been matched.
 *
 */

#include <string>

#include "test_runner.hpp"
#include "../tcli/tcli.hpp"

struct parse_parameter_tag {};
using Tests = test::test_runner<parse_parameter_tag,200>;

namespace test {

using str_vec = std::vector<std::string>;

/**
 * \brief Environment setup for the single parameter parse tests
 *
 * Single parameter parse should read one argument and convert it to the given
 * data vie operator>>.
 *
 * Expected behaviour:
 *   - the argument iterator is moved one step forward
 *   - Data::operator>> is used to read the string given
 *   - if the string is not read in its entirety, a partial_parse exception is
 *     thrown.
 *   - returns true
 */
struct single_param_env {
    struct data {
        int value;
        bool op_in_called = false;
        friend std::istream& operator>>(std::istream& is, data& d) {
            is >> d.value;
            d.op_in_called = true;
            return is;
        }
    };

    struct param_desc {
        using type = data;
        std::vector<std::string> flags = {"--flag"};
    };

    template<class T>
    struct fake_handle {};

    str_vec args = {"--flag", "42", "after"};
    str_vec::iterator current_arg = args.begin() + 1;
    fake_handle<param_desc> handle;
    data d;

};


// Check that the argument iterator moved one step forward
template<> template<>
void Tests::test<0>() {
    single_param_env env;
    const auto it = env.current_arg;
    try {
        tcli::detail::parse_parameter(env.handle, env.d, env.args, env.current_arg);
    } catch(std::exception& e) {
        assert(false, std::string("An unexpected exception was thrown: ") + e.what());
    }
    assert_equal(env.current_arg - it, 1, "Iterator has not moved correctly.");
    assert_equal(*env.current_arg, "after", "Iterator has not moved correctly.");
}

// Check that operator>> is used
template<> template<>
void Tests::test<1>() {
    single_param_env env;
    try {
        tcli::detail::parse_parameter(env.handle, env.d, env.args, env.current_arg);
    } catch(std::exception& e) {
        assert(false, std::string("An unexpected exception was thrown: ") + e.what());
    }
    assert(env.d.op_in_called, "operator>> was not called.");
}

// Check that operator>> is used
template<> template<>
void Tests::test<2>() {
    single_param_env env;
    env.args[1] = "3.14";
    try {
        tcli::detail::parse_parameter(env.handle, env.d, env.args, env.current_arg);
        assert(false, "Exception was not thrown");
    } catch(tcli::partial_parse) {}
}

// Check that parse_parmeter returns true
template<> template<>
void Tests::test<3>() {
    single_param_env env;
    bool ret;
    try {
        ret = tcli::detail::parse_parameter(env.handle, env.d, env.args, env.current_arg);
    } catch(std::exception& e) {
        assert(false, std::string("An unexpected exception was thrown: ") + e.what());
    }
    assert(ret, "Return is false");
}


/**
 * \brief Environment setup for the flagged parameter parse tests
 *
 * Flagged parameter parse should allow enabling or disabling boolean data.
 *
 * Expected behaviour:
 *   - current argument iterator is not moved
 *   - boolean value is set to true except if (current_argument-1) ends with '=off'
 *   - returns true
 */
struct flag_param_env {
    struct param_desc {
        using type = bool;
        str_vec flags = {"--flag"};
        enum {flagged};
    };

    template<class T>
    struct fake_handle {};

    str_vec args = {"before", "--flag", "current", "after"};
    str_vec::iterator current_arg = args.begin() + 2;
    fake_handle<param_desc> handle;
    bool d;
};

// Check that current iterator is not moved
template<> template<>
void Tests::test<50>() {
    flag_param_env env;
    const auto it = env.current_arg;
    tcli::detail::parse_parameter(env.handle, env.d, env.args, env.current_arg);
    assert_equal(env.current_arg - it, 0, "Iterator has moved.");
}

// Check that boolean value is set to false if the flag ends with '=off'
template<> template<>
void Tests::test<51>() {
    flag_param_env env;
    env.d = true;
    env.args[1] = "--flag=off";
    tcli::detail::parse_parameter(env.handle, env.d, env.args, env.current_arg);
    assert_equal(env.d, false, "Data was not set correctly");
}

// Check that boolean value is not set to false if '=off' appears in the flag
// but not at the end
template<> template<>
void Tests::test<52>() {
    flag_param_env env;
    env.d = false;
    env.args[1] = "--flag=off=on";
    tcli::detail::parse_parameter(env.handle, env.d, env.args, env.current_arg);
    assert_equal(env.d, true, "Data was not set correctly");
}

// Check that parse_parmeter returns true
template<> template<>
void Tests::test<53>() {
    flag_param_env env;
    bool ret = tcli::detail::parse_parameter(env.handle, env.d, env.args, env.current_arg);
    assert(ret, "Return is false");
}

/**
 * \brief Environment setup for parameter with a custom parse method
 *
 * Expected behaviour:
 *   - call descriptor parser method
 *   - returns the same as the parse method of the descriptor
 */
struct custom_parse_parameter_env {
    struct data {
        double x = 0, y = 0;
        bool used_parse = false;
    };

    static bool parse_ret;

    struct param_desc {
        using type = data;
        str_vec flags = {"--flag"};

        static bool parse(str_vec&, str_vec::iterator& cur, data& d) {
            d.used_parse = true;
            std::stringstream sstr;
            sstr.str(*cur);
            sstr >> d.x;
            ++cur;
            sstr.str(*cur);
            sstr >> d.y;
            ++cur;
            return parse_ret;
        }
    };

    template<class T>
    struct fake_handle {
        T descriptor;
    };

    str_vec args = {"before", "--flag", "1.2", "3.4", "after"};
    str_vec::iterator current_arg = args.begin() + 2;
    fake_handle<param_desc> handle;
    data d;
};

bool custom_parse_parameter_env::parse_ret = true;

// Check that custom parse method is called
template<> template<>
void Tests::test<100>() {
    custom_parse_parameter_env env;
    tcli::detail::parse_parameter(env.handle, env.d, env.args, env.current_arg);
    assert(env.d.used_parse, "Custom parse method was not called.");
}

// Check that return is the same as custom parse method, case true
template<> template<>
void Tests::test<101>() {
    custom_parse_parameter_env env;
    bool ret;
    custom_parse_parameter_env::parse_ret = true;
    ret = tcli::detail::parse_parameter(env.handle, env.d, env.args, env.current_arg);
    assert_equal(ret, custom_parse_parameter_env::parse_ret, "Wrong return result.");
}

// Check that return is the same as custom parse method, case false
template<> template<>
void Tests::test<102>() {
    custom_parse_parameter_env env;
    bool ret;
    custom_parse_parameter_env::parse_ret = false;
    ret = tcli::detail::parse_parameter(env.handle, env.d, env.args, env.current_arg);
    assert_equal(ret, custom_parse_parameter_env::parse_ret, "Wrong return result.");
}

}

int main() {
    return Tests{}.run();
}
