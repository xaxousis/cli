/**
 * \brief Test TCLI parameter_data class
 * \file
 *
 * parameter_data is a handler that helps extracting the type for a parameter
 * descriptor. It allows handling transparently single parameters, which store
 * one instance of their data type, and stacked parameters, which store multiple
 * such instances.
 */

#include <string>

#include "test_runner.hpp"
#include "../tcli/tcli.hpp"

struct tag {};
using Tests = test::test_runner<tag, 50>;

namespace test {

/// Single parameter descriptor
struct single_desc {
    using type = int;
};

static_assert(tcli::meta::is_stackable<single_desc>::value == false,
              "Single descriptor definition is incorrect");


/// Stacked parameter descriptor
struct stacked_desc {
    using type = float;
    enum {stackable};
};
static_assert(tcli::meta::is_stackable<stacked_desc>::value == true,
              "Stacked descriptor definition is incorrect");


/// Return false if single descriptor container is not the parameter descriptor
/// type
constexpr bool check_single_descriptor_type() {
    return std::is_same<typename tcli::detail::parameter_data<single_desc>::type,
                        typename single_desc::type>
        ::value;
}

/// Check that single descriptor type is correct
template<> template<>
void Tests::test<0>() {
    assert(check_single_descriptor_type(),
           msg("tcli::detail::parameter_data single descriptor container"
               " type is not right.\n Expected ",
               typeid(typename single_desc::type).name(), "\nGot ",
               typeid(typename tcli::detail::parameter_data<single_desc>::type).name())
        );
}

/**
 * \brief Check single parameter data access
 *
 * Expected result: the data returned is always the same
 */
template<> template<>
void Tests::test<1>() {
    using p_data = tcli::detail::parameter_data<single_desc>;

    typename p_data::type data_container;
    auto& d = p_data::get(data_container);
    auto& e = p_data::get(data_container);
    assert(&d == &e, msg("Consecutive calls to 'get' for a single"
                         " parameter do not return the same object."));
}



/// Return false if stacked descriptor container is not a vector of the
/// parameter descriptor type
constexpr bool check_stacked_descriptor_type() {
    return std::is_same<typename tcli::detail::parameter_data<stacked_desc>::type,
                        std::vector<typename stacked_desc::type> >
        ::value;
}


/// Check that stacked descriptor type is correct
template<> template<>
void Tests::test<10>() {
    assert(check_stacked_descriptor_type(),
           msg("tcli::detail::parameter_data stacked descriptor container"
               " type is not right.\n Expected ",
               typeid(std::vector<typename stacked_desc::type>).name(), "\nGot ",
               typeid(typename tcli::detail::parameter_data<stacked_desc>::type).name())
        );
}

/**
 * \brief Check stacked parameter data access
 *
 * Expected result:
 *   - the data returned is always the same
 */
template<> template<>
void Tests::test<11>() {
    using p_data = tcli::detail::parameter_data<stacked_desc>;

    typename p_data::type data_container;
    auto& d = p_data::get(data_container);
    auto& e = p_data::get(data_container);
    assert(&d != &e, msg("Consecutive calls to 'get' for a stacked"
                         " parameter return the same object."));
}

}

int main() {
    return Tests{}.run();
}
