/**
 * \brief Test various unsorted aspects of TCLI
 * \file
 *
 */

#include <string>

#include "test_runner.hpp"
#include "../tcli/tcli.hpp"
#include "../tcli/help_descriptor.hpp"

struct vrac_parameter_tag {};
using Tests = test::test_runner<vrac_parameter_tag,20>;


struct Radius {
    tcli::str_vec flags = {"-R"};
    using type = double;
    enum {required};
};

struct sub_parser : decltype(tcli::make_parser(Radius{})) {
    tcli::str_vec flags = {"--sub"};
};


namespace test {

    struct env {
        tcli::str_vec args = {"test_10","--sub","-R","3.14"};
    };

    template<> template<>
    void Tests::test<0>() {
        env e;
        auto parser = tcli::make_parser(sub_parser{}, tcli::help{});
        parser.get<sub_parser>().get<Radius>();
        parser.parse(e.args);


    }
}

int main() {
    return Tests{}.run();
}
