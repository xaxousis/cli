/**
 * \brief Test parser visit parameter overloads
 * \file
 *
 * The visit_parameter group of functions gives the possibility to interact with
 * the parser once a parameter has been matched.
 *
 */

#include <string>

#include "test_runner.hpp"
#include "../tcli/tcli.hpp"

struct visit_parameter_tag {};
using Tests = test::test_runner<visit_parameter_tag,20>;

using str_vec = std::vector<std::string>;

namespace test {
    /**
     * \brief Environment setup for parameter with a visit method
     *
     * Expected behaviour:
     *   - call descriptor visit method
     */
    struct visit_parameter_env {
        struct param_desc {
            using type = bool;
            str_vec flags = {"--flag"};

            template<class Container, class It, class Parser>
            void visit(Parser& parser, Container&, It&) {
                parser.visited = true;
            }
        };

        template<class T>
        struct fake_handle {
            T descriptor;
        };

        struct fake_parser {
            bool visited = false;
        };

        str_vec args = {"before", "--flag", "1.2", "3.4", "after"};
        str_vec::iterator current_arg = args.begin() + 2;
        fake_handle<param_desc> handle;
        fake_parser parser;
    };

    // Check that visit method is called
    template<> template<>
    void Tests::test<0>() {
        visit_parameter_env env;
        tcli::detail::visit_parameter(env.handle, env.args, env.current_arg, env.parser);
        assert(env.parser.visited, "'visit' method was not called.");
    }

    /**
     * \brief Environment setup for parameter without a visit method
     *
     * Expected behaviour:
     *   - do nothing
     */
    struct no_visit_parameter_env {
        struct param_desc {
            using type = bool;
            str_vec flags = {"--flag"};
        };

        template<class T>
        struct fake_handle {};

        struct fake_parser {
            bool visited = false;
        };

        const str_vec args = {"before", "--flag", "1.2", "3.4", "after"};
        const str_vec::const_iterator current_arg = args.begin() + 2;
        const fake_handle<param_desc> handle = {};
        const fake_parser parser = {};
    };


    // Check that nothing is done
    template<> template<>
    void Tests::test<1>() {
        no_visit_parameter_env env;
        tcli::detail::visit_parameter(env.handle, env.args, env.current_arg, env.parser);
        assert(! env.parser.visited, "'visit' method was called.");
    }


}

int main() {
    return Tests{}.run();
}
