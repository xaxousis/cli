/**
 * \brief Test parser accessors
 * \file
 *
 */

#include <string>

#include "test_runner.hpp"
#include "../tcli/tcli.hpp"

struct parser_tag {};
using Tests = test::test_runner<parser_tag,200>;

using str_vec = std::vector<std::string>;

namespace test {

    struct environment {
        struct param_desc {
            using type = bool;
            str_vec flags = {"--flag"};
        };

        template<class T>
        struct fake_handle {};

        struct fake_parser {
            bool visited = false;
        };

        str_vec args = {"before", "--flag", "1.2", "3.4", "after"};
        str_vec::iterator current_arg = args.begin() + 2;

    };


    // Check that parser.param<T>() returns T&
    template<> template<>
    void Tests::test<0>() {
        environment env;
        auto parser = tcli::make_parser(environment::param_desc{});

        assert(
            std::is_same<
                decltype(parser.param<environment::param_desc>()),
                environment::param_desc&
            >::value,
            "parser.param does not return a reference");
    }

    // Check that parser.get<T>() returns T::type&
    template<> template<>
    void Tests::test<1>() {
        environment env;
        auto parser = tcli::make_parser(environment::param_desc{});

        assert(
            std::is_same<
                decltype(parser.get<environment::param_desc>()),
                bool&
            >::value,
            "parser.param does not return a reference");
    }

    // Check that parser.exists<T>() returns bool
    template<> template<>
    void Tests::test<2>() {
        environment env;
        auto parser = tcli::make_parser(environment::param_desc{});

        assert(
            std::is_same<
                decltype(parser.exists<environment::param_desc>()),
                bool
            >::value,
            "parser.param does not return a reference");
    }



}


int main() {
    return Tests{}.run();
}
