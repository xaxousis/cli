#include "test_runner.hpp"

#include "../tcli/meta.hpp"


struct meta_test_tag {};
using Tests = test::test_runner<meta_test_tag, 50>;

namespace test {

struct empty_t {};

struct test_stackable {
    enum {stackable};
};

struct string_operations {
    string_operations() = default;
    string_operations(const std::string&) {}
    string_operations& operator=(const std::string&) {return *this;}

    friend std::istream& operator>>(std::istream& is, string_operations&){return is;}
};

struct test_parse {
    static bool parse(int, double&) { return {};}
};

struct derived_parse : test_parse {};


// Test is_stackable type trait
template<> template<>
void Tests::test<0>() {
    assert(! tcli::meta::is_stackable<empty_t>::value,
                  "empty_t is seen as stackable");
    assert(tcli::meta::is_stackable<test_stackable>::value,
                  "test_stackable is not seen as stackable");
}

// Test is_istream_settable type trait
template<> template<>
void Tests::test<1>() {
    assert(! tcli::meta::is_istream_settable<empty_t>::value,
           "empty_t is string assigneble");
    assert(tcli::meta::is_istream_settable<string_operations>::value,
           "string_operations is not istream assignable");
}

// Test meta::list class
template<> template<>
void Tests::test<2>() {
    assert(tcli::meta::list<float, int, double>::index<int>::value == 1,
           msg("Wrong int index in list<float,int, double>\nExpected ", 1, ", "
               "got", tcli::meta::list<float, int, double>::index<int>::value));
    assert(tcli::meta::list<int, float, double>::index<int>::value == 0,
           msg("Wrong int index in list<float,int, double>\nExpected ", 0, ", "
               "got", tcli::meta::list<float, int, double>::index<int>::value));
    assert(tcli::meta::list<float, double, int>::index<int>::value == 2,
           msg("Wrong int index in list<float,int, double>\nExpected ", 2, ", "
               "got", tcli::meta::list<float, int, double>::index<int>::value));

    assert(tcli::meta::list<float, int, double>::exists<int>::value,
           "int not found in list<float, int, double>.");
    assert(tcli::meta::list<int, float, double>::exists<int>::value,
           "int not found in list<float, int, double>.");
    assert(tcli::meta::list<float, double, int>::exists<int>::value,
           "int not found in list<float, int, double>.");
    assert(! tcli::meta::list<float, double>::exists<int>::value,
           "int found in list<float, double>.");
}

// Test
template<> template<>
void Tests::test<3>() {
    assert(tcli::meta::has_parse<test_parse, void(int, double&)>::value,
           "Parse method does not exist");
    assert(tcli::meta::has_parse<derived_parse, void(int, double&)>::value,
           "Parse method does not exist");
    assert(! tcli::meta::has_parse<test_parse, int(double&)>::value,
           "Parse method should not exist");
}

}

int main() {
    return Tests{}.run();
}
