# Templated Command Line Interface

A simple c++11 command line parsing library.

## Functionalities

* type safe, every option type is defined at compile time
* automatic help message generation
* sub-parsing capability
* flag options
* stacked options, specify an option several times
* required options
* customisable parse behaviour
* customisable visit behaviour

## Parser creation

Each option is defined via a descriptor. A descriptor is a structure defined as follows.
```cpp
struct option_descriptor {
    using type = T // the option underlying type, required

    std::vector<std::string> names; // long flags, required, size must be > 1
    std::vector<std::string> flags; // short flags, required

    std::string description; // help message description

    type def; // default value when option is not specified, optional
              // absence does not make option required

    enum {
        stackable, // the option may be specified several times, optional
        required,  // the option must be specified at least once, optional
        flagged,   // the option does not take an additional argument
        hidden     // the option must not appear in the help message
    };
};
```

To create a parser, call the `make_parser` function or create the parser by hand.
```cpp
auto parser = tcli::make_parser(option_descriptor_1{}, option_descriptor_1{});
using parser_t = parser<option_descriptor_1, option_descriptor_2>;
// decltype(parser) == parser_t
```

To create a sub-parser, create the first parser and pass it as an option descriptor.
```cpp
auto sub_parser = tcli::make_parser("sub-parser-flag", option_descriptor{});
auto parser = tcli::make_parser(sub_parser);
```

You can add a description to the parser through it description public member.
```cpp
parser.description = "A parser description";
```

## Parse
To parse the command line, pass `argc` and `argv` to the parser.
```cpp
parser.parse(argc, argv);
```

## Retrieve options
Retrives the parse results using the `get` method and the descriptor type. If the descriptor holds the `stackable` value, `get` returns a `std::vector` of options (which may be empty), otherwise it returns the data.
For simple arguments, the signature is:
```cpp
// descriptor::stackable does not exist
typename descriptor::type& get();
// descriptor::stackable exists
std::vector<typename descriptor::type>& get();
// call the get method
parser.get<option_descriptor>();
```
