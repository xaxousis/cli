#ifndef TCLI_SFINAE_HPP
#define TCLI_SFINAE_HPP

#include <istream>
#include <type_traits>
#include <string>
#include <vector>

namespace tcli {
    namespace meta {

        template<class...>
        using void_t = void;

        template<class...>
        using bool_t = bool;

        template<class...>
        struct true_t {
            enum {value = true};
        };

        template<bool B, bool... Bs>
        struct all_true {
            template<bool... Cs> struct list{};
            enum {value = std::is_same<list<B,Bs...>, list<Bs...,B> >::value};
        };

        template<class T, class U = char>
        using use_if = typename std::enable_if<T::value, U>::type;

        template<class T, class U = char>
        using not_use_if = typename std::enable_if<!T::value, U>::type;

        template<class T>
        struct is_stackable {
            template<class U, int = U::stackable>
            static constexpr bool check(U*) {return true;}
            static constexpr bool check(...) {return false;}

            enum {value = check((T*)0)};
        };

        template<class T>
        struct is_parser {
            template<class U, int = U::tcli_parser>
            static constexpr bool check(U*) {return true;}
            static constexpr bool check(...) {return false;}

            enum {value = check((T*)0)};
        };

        template<class T>
        struct is_flagged {
            template<class U, int = U::flagged>
            static constexpr bool check(U*) {return true;}
            static constexpr bool check(...) {return false;}

            enum {value = check((T*)0)};
        };

        template<class T>
        struct is_required {
            template<class U, int = U::required>
            static constexpr bool check(U*) {return true;}
            static constexpr bool check(...) {return false;}

            enum {value = check((T*)0)};
        };


        template<class T>
        struct is_istream_settable {
            template<class, class = void>
            struct check : std::false_type {};
            template<class U>
            struct check<U, void_t<decltype(std::declval<std::istream&>() >> std::declval<U&>())> >
                : std::true_type {};
            enum {value = check<T>::value};
        };


        template<class T>
        struct has_type {
            template<typename U>
            static constexpr bool check(U*, typename U::type* = nullptr) {return true;}
            static constexpr bool check(...) {return false;}

            enum {value = check((T*)0)};
        };


        template<class T, class Func>
        struct has_parse;

        template<class T, class Ret, class... Args>
        struct has_parse<T, Ret(Args...)> {
            template<class, class = void>
            struct check : std::false_type {};

            template<class U>
            struct check<U, void_t<decltype(U::parse(std::declval<Args>()...))> >
                : std::true_type {};

            enum {value = check<T>::value};
        };

        template<class T, class Func>
        struct has_visit;

        template<class T, class Ret, class... Args>
        struct has_visit<T, Ret(Args...)> {
            template<class, class = void>
            struct check : std::false_type {};

            template<class U>
            struct check<U, void_t<decltype(std::declval<U>().visit(std::declval<Args>()...))> >
                : std::true_type {};

            enum {value = check<T>::value};
        };



        template<class T>
        struct has_default {
            template<class U, decltype(std::declval<U>().def)* = nullptr>
            static constexpr std::true_type check(U*) {return {};}
            static constexpr std::false_type check(...) {return {};}

            enum {value = decltype(check((T*)nullptr))::value};
        };


        template<class... List>
        struct list {

            enum {size = sizeof...(List)};

            template<class U, class... Vs>
            struct find;

            template<class U>
            struct find<U> {
                static constexpr int f(int i) {
                    return i+1;
                }
            };

            template<class U, class V, class... Vs>
            struct find<U,V,Vs...> {
                static constexpr int f(int i) {
                    return std::is_same<U,V>::value ? i : find<U,Vs...>::f(i+1);
                }
            };

            template<class T>
            struct index {
                enum {value = find<T, List...>::f(0)};
            };

            template<class T>
            struct exists {
                enum {value = (find<T, List...>::f(0) < size)};
            };
        };

    }
}



#endif /* TCLI_SFINAE_HPP */
