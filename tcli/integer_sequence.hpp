#ifndef CPP_INTEGER_SEQUENCE_POLYFILL_HPP__
#define CPP_INTEGER_SEQUENCE_POLYFILL_HPP__

/** Integer sequence type
 * \file
 *
 * \author Quentin Khan
 *
 * This file implements the c++14 std::integer_sequence class. It can be use as
 * a polyfill when compiling with c++11 standard.
 */


#if __cplusplus < 201103L
#error "The definition of integer_sequence needs C++11."
#endif


#if __cplusplus >= 201402L

#include <utility>
namespace cpp {
    using namespace std;
}

#else

#include <cstddef> // for std::size_t
#include <ostream>

namespace cpp {

    /** \brief Recursive definition of the integer_sequence type
     *
     * \author Quentin Khan
     *
     * This structure allows using template parameter deduction to expand variadic
     * template parameter packs using indexes. One such use is to apply a function
     * to every element in a std::tuple. Another use case is to extract all elements
     * from a tuple to a function parameters.
     *
     * Use \link make_integer_sequence \endlink to create an object instance.
     *
     * Example:
     * ~~~~{cpp}
     * // The max function takes a variadic count of arguments and returns the max.
     * template<class... Args>
     * double max(const Args&... args);
     *
     * // The helper calls the max function with the expanded tuple
     * template<class... Args, std::size_t... Indexes>
     * double tuple_max_helper(const std::tuple<Args...>& tuple, std::integer_sequence<Indexes...>) {
     *      // std::get<Indexes>(tuple)... is expanded into 'std::get<0>(tuple), std::get<1>(tuple), ...'
     *      return max( std::get<Indexes>(tuple)... );
     * }
     *
     * template<class... Args>
     * double tuple_max(const std::tuple<Args...>& tuple) {
     *     tuple_max_helper(tuple, std::make_integer_sequence<sizeof...(Args)>());
     * }
     * ~~~~
     *
     * \tparam T Underlying type for the sequence
     * \tparam Ints Sequence of integers of type T
     */
    template<typename T, T... Ints>
    class integer_sequence {
        /** \brief integer_sequence length
         * \return The interger count int the integer_sequence
         */
        static constexpr std::size_t size() noexcept {
            return sizeof...(Ints);
        }
    };

    /** \brief Shorthand for integer_sequence of type std::size_t
     */
    template<std::size_t... Ints>
    using index_sequence = integer_sequence<std::size_t, Ints...>;

    namespace detail {

        /** \brief Recursive definition of the integer_sequence type
         * \copydetails integer_sequence
         */
        template<typename T, std::size_t I, T... Is>
        struct integer_sequence_impl {
            using type = typename integer_sequence_impl<T, I-1, static_cast<T>(I-1), Is...>::type;
        };

        /** \brief Recursive definition end of the integer_sequence type
         * \copydetails integer_sequence
         */
        template<typename T, T... Is>
        struct integer_sequence_impl<T, 0, Is...> {
            using type = integer_sequence<T, Is...>;
        };

        template<typename T, T N>
        struct _make_integer_sequence {
            static_assert(N >= 0, "Cannot create integer sequence of negative length.");
            using type = typename integer_sequence_impl<T, static_cast<T>(N)>::type;
        };

    }

    /** \brief integer_sequence<T, 0, ..., N-1> type.
     * \tparam Base integer type.
     * \tparam N Suquence upper bound.
     */
    template<class T, T N>
    using make_integer_sequence = typename detail::_make_integer_sequence<T,N>::type;

    /** \brief index_sequence<0, ..., N-1> type.
     * \tparam N Sequence upper bound.
     */
    template<std::size_t N>
    using make_index_sequence = make_integer_sequence<std::size_t, N>;

    /** \brief index_sequence type corresponding to a type pack.
     *
     */
    template<class... T>
    using index_sequence_for = make_index_sequence<sizeof...(T)>;

}
#endif

namespace cpp {
    template<typename T, T... Ints>
    std::ostream& operator<<(std::ostream& os, integer_sequence<T, Ints...>) {
        auto l = {0, ((os << Ints << (Ints != sizeof...(Ints)-1 ? " " : "")),0) ...};
        (void)l;
        return os;
    }

}





#endif // INTEGER_SEQUENCE_POLYFILL_HPP__
